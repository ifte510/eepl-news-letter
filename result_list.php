<?php require_once("functions.php"); ?>
<?php require_once("header.php"); ?>
<?php require_once("csv_to_array.php"); ?>

    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <header>
            <div class="container">
                <div class="logo">
                    <img src="img/logo.jpg" alt="">
                </div>
                <nav class="navbar navbar-default" role="navigation">
                    <ul class="nav navbar-nav">
                      
                      <li>
                        <a href="http://eagle-empire.com">
                         EEPL Home
                        </a>
                      </li>
                      <li>
                        <a href="http://support.eagle-empire.com">
                         Support Center
                        </a>
                      </li>
                    </ul>
                </nav>
            </div><!-- .container -->

        </header>


        <div class="container">

            <div class="row">
                <div class="col-md-4">
                    <?php get_sidebar(); ?>
                </div><!-- .col-md-4 -->
                <div class="col-md-8">
                    <?php 


                        $select_db_mail = "SELECT * FROM email";
                        $select_db_mail .= " WHERE group_id= {$_GET['group_id']}";

                        $query_result = mysql_query($select_db_mail);
                        if (!$query_result) {
                            die("mysql query error" . mysql_error());
                        }
                    ?>
                    <ul>
                        <?php
                        while ($query_result_row = mysql_fetch_array($query_result)) { ?>
                            <li><?php echo $query_result_row['mail_address']; ?> <a href="delete_mail.php?mail_group_id=<?php echo $_GET['group_id']; ?>&mail_address=<?php echo $query_result_row['mail_address']; ?>&mail_id=<?php echo $query_result_row['id']; ?>">Delete</a></li>
                        <?php }
                         ?>
                    </ul>
                </div><!-- .col-md-8 -->
            </div><!-- .row -->
        </div><!-- .container -->
		
<?php require_once("footer.php"); ?>