<?php require_once("functions.php"); ?>
<?php require_once("header.php"); ?>
<?php require_once("csv_to_array.php"); ?>


<?php 


if(isset($_POST['client_group_add_submit'])){

  $client_group_add = $_POST['client_group_add'];

  $query = "SELECT * FROM";
    $query .=" client_group";
    $query .= " WHERE";
    $query .= " group_name = '$client_group_add'";

    $result=mysql_query($query);

    if (!$result) {
      die("Database query failed ".mysql_error());
    }

    $num = mysql_num_rows($result);

    if($num==0){

      $mail_insert= "INSERT INTO";
      $mail_insert .= " client_group (group_name)";
      $mail_insert .=" VALUES ('{$client_group_add}')";
      
      $mail_insert_res=mysql_query($mail_insert);

      if (!$mail_insert_res) {
        die("Database query failed ".mysql_error());
      }

      $insertion_message[] = "Newsletter list created successfully";

    }else{
      $insertion_message[] = "Newsletter list <strong>{$client_group_add}</strong> already exists";
    }

}


 ?>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <!-- Add your site or application content here -->
        <header>
            <div class="container">
                <div class="logo">
                    <img src="img/logo.jpg" alt="Eagle Empire Pacific Ltd.">
                </div>
                <nav class="navbar navbar-default" role="navigation">
                    <ul class="nav navbar-nav">
                      
                      <li>
                        <a href="http://eagle-empire.com">
                         EEPL Home
                        </a>
                      </li>
                      <li>
                        <a href="http://support.eagle-empire.com">
                         Support Center
                        </a>
                      </li>
                    </ul>
                </nav>
            </div><!-- .container -->

        </header>

        <div class="container">

            <div class="row">

                <div class="col-md-4">

                    <?php get_sidebar(); ?>

                </div><!-- col-md-4 -->

                <div class="col-md-8">
                    <div class="main-content">

                      <?php 

                         if(!empty($insertion_message)){ ?>


                            <div class="panel panel-primary">
                              <div class="panel-heading">
                                <h3 class="panel-title">Attention!</h3>
                              </div>
                              <div class="panel-body">
                                <?php 

                                  echo "<ol>";
                                  
                                  for ($i=0; $i <sizeof($insertion_message); $i++) { 
                                    
                                    echo "<li>".$insertion_message[$i]."</li>";
                                  
                                  }
                                  
                                  echo "</ol>";
                                ?>

                              </div>
                            </div>


                         <?php 
                         
                         }
                        
                       ?>
                        <form class="form-horizontal" action="create_mail_list.php" role="form" method="post" id="add_mail_form">
                            

                            

                            <div class="form-group">
                                <label for="client_group_add" class="col-sm-3 control-label">Newsletter List</label>
                                <div class="col-sm-9">
                                  <p><input type="text" name="client_group_add" id="client_group_add" class="form-control" placeholder="Newsletter List"></p>
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                  <input type="submit" name="client_group_add_submit" class="btn btn-primary" value="Add New ">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div><!-- row -->
            
        </div><!-- container -->
    

<?php require_once("footer.php"); ?>