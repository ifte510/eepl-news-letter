<?php require_once("functions.php"); ?>
<?php require_once("header.php"); ?>
<?php require_once("csv_to_array.php"); ?>


<?php 


if(isset($_POST['add_department'])){

$department_name=ucwords($_POST['add_department_name']);
$department_mail=$_POST['add_department_mail'];

  $department_query = "SELECT * FROM";
  $department_query .=" department";
  $department_query .= " WHERE";
  $department_query .= " department_name = '{$department_name}'";
  $department_query .= " AND department_mail = '{$department_mail}'";
  
  $department_query_result=mysql_query($department_query);
  if (!$department_query_result) {
      die("Database query failed ".mysql_error());
    }

    $department_query_num = mysql_num_rows($department_query_result);

    if($department_query_num==0){

      $department_mail_insert= "INSERT INTO";
      $department_mail_insert .= " department (department_name, department_mail)";
      $department_mail_insert .=" VALUES ('{$department_name}','{$department_mail}')";

      $department_mail_insert_res=mysql_query($department_mail_insert);

      if (!$department_mail_insert_res) {
        die("Database query failed ".mysql_error());
      }

      $insertion_message = "Department added successfully";
    }else{
      $insertion_message = "Department address already exists";
    }
}

 ?>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <!-- Add your site or application content here -->
        <header>
            <div class="container">
                <div class="logo">
                    <img src="img/logo.jpg" alt="Eagle Empire Pacific Ltd.">
                </div>
                <nav class="navbar navbar-default" role="navigation">
                    <ul class="nav navbar-nav">
                      
                      <li>
                        <a href="http://eagle-empire.com">
                         EEPL Home
                        </a>
                      </li>
                      <li>
                        <a href="http://support.eagle-empire.com">
                         Support Center
                        </a>
                      </li>
                    </ul>
                </nav>
            </div><!-- .container -->

        </header>

        <div class="container">

            <div class="row">

                <div class="col-md-4">

                    <?php get_sidebar(); ?>

                </div><!-- col-md-4 -->

                <div class="col-md-8">
                    <div class="main-content">
                      <?php if(!empty($insertion_message)){ ?>
                          <div class="panel panel-primary">
                              <div class="panel-heading">
                                <h3 class="panel-title">Attention!</h3>
                              </div>
                              <div class="panel-body">
                                <?php 

                                
                                  echo $insertion_message;
                                
                                  
                                ?>

                              </div>
                            </div>
                            <?php } ?>
                        <form class="form-horizontal" action="add_department.php" role="form" method="post" id="add_mail_form">
                            

                            <div class="form-group">
                              <label for="add_department_name" class="col-sm-3 control-label">Department Name</label>
                              <div class="col-sm-9">
                                <p><input type="text" name="add_department_name" id="add_department_name" class="form-control" placeholder="Department Name" required></p>
                              </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label for="add_department_mail" class="col-sm-3 control-label">Email Address</label>
                                <div class="col-sm-9">
                                  <p><input type="text" name="add_department_mail" id="add_department_mail" class="form-control" placeholder="Email Address" required></p>
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                  <input type="submit" name="add_department" class="btn btn-primary" value="Add Department">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div><!-- row -->
            
        </div><!-- container -->
    

<?php require_once("footer.php"); ?>