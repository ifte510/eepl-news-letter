<?php require_once("functions.php"); ?>
<?php require_once("header.php"); ?>
<?php require_once("csv_to_array.php"); ?>


<?php 


if(isset($_POST['mail_Submit'])){

    $mail_cc_addresses=explode(",", trim($_POST['cc_add']));
    $mail_bcc_addresses=explode(",", trim($_POST['bcc_add']));

    if(isset($_FILES['file_upload'])){
        $file_name_array=$_FILES['file_upload']['name'];
        $temp_name_array=$_FILES['file_upload']['tmp_name'];
    }
    

    if($_POST['mail_type']=='single_mail'){

        $mail = new PHPMailer;

        $mail_dest_addresses=explode(",", trim($_POST['dest_add']));
        $mail->From = $_POST['email_source'];
        $mail->FromName = $_POST['email_source_name'];

        foreach ($mail_dest_addresses as $mail_dest_address) {
            $mail->addAddress($mail_dest_address);
        }

        $mail->addReplyTo($_POST['email_source']);
        foreach ($mail_cc_addresses as $mail_cc_address){
            $mail->addCC($mail_cc_address);
        }
        foreach ($mail_bcc_addresses as $mail_bcc_address){
            $mail->addBCC($mail_bcc_address);
        }

        $mail->WordWrap = 50;
        $mail->isHTML(true);  

        if(!empty($temp_name_array))  {                              // Set email format to HTML
        for($i=0; $i<count($temp_name_array); $i++){

            if(move_uploaded_file($temp_name_array[$i], 'files/'.$file_name_array[$i])){
                $mail->addAttachment('files/'.$file_name_array[$i], $file_name_array[$i]);
            }
            
        }
    }
        $mail->isHTML(true);                              
        $mail->Subject = $_POST['subject'];
        $mail->Body    = $_POST['message'];

        if(!$mail->send()) {

            $mess= '<div class="panel panel-primary">
                          <div class="panel-heading">
                            <h3 class="panel-title">Attention!</h3>
                          </div>
                          <div class="panel-body">
                            <p>Message could not be sent.</p>
                            <p>Mailer Error:'.$mail->ErrorInfo.'</p>
                          </div>
                        </div>';
        } else {

            $mess= '<div class="panel panel-primary">
                          <div class="panel-heading">
                            <h3 class="panel-title">Attention!</h3>
                          </div>
                          <div class="panel-body">
                            Message has been sent
                          </div>
                        </div>';
        }
    }else if($_POST['mail_type']=='bulk_mail'){
        $eepl_email_list_db="SELECT *";
        $eepl_email_list_db .= " FROM email";
        $eepl_email_list_db .=" WHERE group_id = '{$_POST['news_letter_list']}'";

        $eepl_email_list_query = mysql_query($eepl_email_list_db);
        if (!$eepl_email_list_query ) {
            die("Database query error ". mysql_error());
        }
        while ($bulk_mail_lists = mysql_fetch_array($eepl_email_list_query)) {




            //check if blocklist is provided

            if(isset($_POST['email_blocklist']) || !empty($_POST['email_blocklist'])){
                
                $email_block_lists=explode(",", trim($_POST['email_blocklist']));

                foreach ($email_block_lists as $email_block_list){

                    if ($email_block_list !=$bulk_mail_lists['mail_address']) {
                        

                        $mail = new PHPMailer;

                        // $mail_dest_addresses=explode(",", trim($_POST['dest_add']));
                        $mail->From = $_POST['email_source'];
                        $mail->FromName = $_POST['email_source_name'];
                        $mail->addAddress($bulk_mail_lists['mail_address']);

                        $mail->WordWrap = 50;
                        $mail->isHTML(true); 

                        $mail->Subject = $_POST['subject'];
                        $mail->Body    = $_POST['message'];

                        if(!$mail->send()) {
                        $mess= '<div class="panel panel-primary">
                                      <div class="panel-heading">
                                        <h3 class="panel-title">Attention!</h3>
                                      </div>
                                      <div class="panel-body">
                                        <p>Message could not be sent.</p>
                                        <p>Mailer Error:' . $mail->ErrorInfo.'</p>
                                      </div>
                                    </div>';
                        } else {
                            $mess= '<div class="panel panel-primary">
                                      <div class="panel-heading">
                                        <h3 class="panel-title">Attention!</h3>
                                      </div>
                                      <div class="panel-body">
                                        Message has been sent
                                      </div>
                                    </div>';
                        }





                    }
                }

            }else{
                $mail = new PHPMailer;

                // $mail_dest_addresses=explode(",", trim($_POST['dest_add']));
                $mail->From = $_POST['email_source'];
                $mail->FromName = $_POST['email_source_name'];
                $mail->addAddress($bulk_mail_lists['mail_address']);

                $mail->WordWrap = 50;
                $mail->isHTML(true); 

                $mail->Subject = $_POST['subject'];
                $mail->Body    = $_POST['message'];

                if(!$mail->send()) {
                $mess= '<div class="panel panel-primary">
                              <div class="panel-heading">
                                <h3 class="panel-title">Attention!</h3>
                              </div>
                              <div class="panel-body">
                                <p>Message could not be sent.</p>
                                <p>Mailer Error:' . $mail->ErrorInfo.'</p>
                              </div>
                            </div>';
                } else {
                    $mess= '<div class="panel panel-primary">
                              <div class="panel-heading">
                                <h3 class="panel-title">Attention!</h3>
                              </div>
                              <div class="panel-body">
                                Message has been sent
                              </div>
                            </div>';
                }
            }

        }//while loop
    }
}



 ?>




    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <header>
            <div class="container">
                <div class="logo">
                    <img src="img/logo.jpg" alt="">
                </div>
                <nav class="navbar navbar-default" role="navigation">
                    <ul class="nav navbar-nav">
                      
                      <li>
                        <a href="http://eagle-empire.com">
                         EEPL Home
                        </a>
                      </li>
                      <li>
                        <a href="http://support.eagle-empire.com">
                         Support Center
                        </a>
                      </li>
                    </ul>
                </nav>
            </div><!-- .container -->

        </header>


        <div class="container">

            <div class="row">
                <div class="col-md-4">
                    <?php get_sidebar(); ?>
                </div><!-- .col-md-4 -->
                <div class="col-md-8">

                    <?php if(!empty($mess)){
                        echo $mess;
                    } ?>
                    <form class="form-horizontal" action="index.php" enctype="multipart/form-data" role="form" method="post" id="mail-form">

                        <div class="form-group">
                            <label for="email_source" class="col-sm-2 control-label">Email Form</label>
                            <div class="col-sm-10">
                              <select name="email_source" id="email_source" class="form-control" required>

                                <?php 

                                    $department_query = mysql_query("SELECT * FROM department");
                                    if (!$department_query) {
                                    die("Database query failed");
                                    }
                                    while ($department_row=mysql_fetch_array($department_query)) {
                                        echo '<option value="';
                                        echo $department_row['department_mail'];
                                        echo '">';
                                        echo ucfirst($department_row['department_name']);
                                        echo'</option>';
                                    }


                                 ?>

                              </select>
                            </div>
                        </div><!-- form-group -->


                        <div class="form-group">
                            <label for="email_source_name" class="col-sm-2 control-label">Department Name</label>
                            <div class="col-sm-10">
                              <select name="email_source_name" id="email_source_name" class="form-control" required>

                                <?php 

                                    $department_query = mysql_query("SELECT * FROM department");
                                    if (!$department_query) {
                                    die("Database query failed");
                                    }
                                    while ($department_row=mysql_fetch_array($department_query)) {
                                        echo '<option value="';
                                        echo ucfirst($department_row['department_name']);
                                        echo '">';
                                        echo ucfirst($department_row['department_name']);
                                        echo'</option>';
                                    }


                                 ?>

                              </select>
                            </div>
                        </div><!-- form-group -->

                        
                        <div class="form-group">
                            <label for="mail_type" class="col-sm-2 control-label">Mail Type</label>
                            <div class="col-sm-10">
                              <select name="mail_type" id="mail_type" class="form-control">
                                <option value="single_mail" selected>To single address</option>
                                <option value="bulk_mail">Bulk Email</option>
                              </select>
                            </div>
                        </div><!-- form-group -->

                        <div class="form-group mail_to_field">
                            <label for="news_letter_list" class="col-sm-2 control-label">Email To</label>
                            <div class="col-sm-10">
                              <select name="news_letter_list" id="news_letter_list" class="form-control" required>

                                <?php 

                                    $news_letter_list_query = mysql_query("SELECT * FROM client_group");
                                    if (!$news_letter_list_query) {
                                    die("Database query failed");
                                    }
                                    while ($news_letter_list_row=mysql_fetch_array($news_letter_list_query)) {
                                        echo '<option value="';
                                        echo $news_letter_list_row['id'];
                                        echo '">';
                                        echo ucfirst($news_letter_list_row['group_name']);
                                        echo'</option>';
                                    }


                                 ?>

                              </select>
                            </div>
                        </div><!-- form-group -->


                        <div class="form-group email_blocklist_field">
                            <label for="email_blocklist" class="col-sm-2 control-label">Email Blocklist</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="email_blocklist" name="email_blocklist" placeholder="Email Blocklist">
                            </div>
                        </div><!-- form-group -->

                        <div class="form-group dest_mail_field">
                            <label for="dest_add" class="col-sm-2 control-label">Destination Email</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="dest_add" name="dest_add" placeholder="Destination Email">
                            </div>
                        </div><!-- form-group -->

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                              <a href="#" class="btn btn-default" id="enable_cc">Add CC</a>
                              <a href="#" class="btn btn-default" id="enable_bcc">Add BCC</a>
                            </div>
                        </div><!-- form-group -->

                        <div class="form-group cc_field" style="display:none;" >
                            <label for="cc_add" class="col-sm-2 control-label">CC</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="cc_add" name="cc_add" placeholder="CC">
                            </div>
                        </div><!-- form-group -->

                        <div class="form-group bcc_field" style="display:none;">
                            <label for="bcc_add" class="col-sm-2 control-label">BCC</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="bcc_add" name="bcc_add" placeholder="BCC">
                            </div>
                        </div><!-- form-group -->

                        <div class="form-group">
                            <label for="subject" class="col-sm-2 control-label">Subject</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject">
                            </div>
                        </div><!-- form-group -->

                        <div class="form-group">
                            <label for="message" class="col-sm-2 control-label">Message</label>
                            <div class="col-sm-10">
                              <textarea name="message" id="message" class="form-control" cols="30" rows="10"></textarea>
                            </div>
                        </div><!-- form-group -->

                        <div class="form-group">
                            <label for="file_upload" class="col-sm-2 control-label">Attachment</label>
                            <div class="col-sm-10 attachment_area">

                            </div>
                        </div><!-- form-group -->

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                              <a href="#" class="btn btn-success" id="enable_attachment">Add Attachment</a>
                            </div>
                        </div><!-- form-group -->


                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                              <input type="submit" name="mail_Submit" class="btn btn-primary" value="Send">
                            </div>
                        </div>

                    </form>
                </div><!-- .col-md-8 -->
            </div><!-- .row -->
        </div><!-- .container -->
		
<?php require_once("footer.php"); ?>