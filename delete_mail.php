<?php require_once("functions.php"); ?>
<?php require_once("header.php"); ?>
<?php require_once("csv_to_array.php"); ?>

    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <header>
            <div class="container">
                <div class="logo">
                    <img src="img/logo.jpg" alt="">
                </div>
                <nav class="navbar navbar-default" role="navigation">
                    <ul class="nav navbar-nav">
                      
                      <li>
                        <a href="http://eagle-empire.com">
                         EEPL Home
                        </a>
                      </li>
                      <li>
                        <a href="http://support.eagle-empire.com">
                         Support Center
                        </a>
                      </li>
                    </ul>
                </nav>
            </div><!-- .container -->

        </header>


        <div class="container">

            <div class="row">
                <div class="col-md-4">
                    <?php get_sidebar(); ?>
                </div><!-- .col-md-4 -->
                <div class="col-md-8">
                    <?php 

                        $gr_id=$_GET['group_id'];

                        $delete_db_mail = "DELETE FROM email";
                        $delete_db_mail .= " WHERE id= {$_GET['mail_id']} LIMIT 1";

                        $query_result = mysql_query($delete_db_mail);
                        if(mysql_affected_rows()==1){
                            redirect_to("newsletter/result_list.php?group_id={$_GET['mail_group_id']}");
                        }
                    ?>
                    
                </div><!-- .col-md-8 -->
            </div><!-- .row -->
        </div><!-- .container -->
		
<?php require_once("footer.php"); ?>