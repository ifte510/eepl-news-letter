<?php require_once("functions.php"); ?>
<?php require_once("header.php"); ?>
<?php require_once("csv_to_array.php"); ?>


<?php 


if(isset($_POST['mail_list_file_upload'])){

  //File Upload Error

    $upload_errors = array(
        UPLOAD_ERR_OK=>'No Errors.',
        UPLOAD_ERR_INI_SIZE=>'Larger than upload_max_filesize',
        UPLOAD_ERR_FORM_SIZE=>'Larger than form MAX_FILE_SIZE.',
        UPLOAD_ERR_PARTIAL=>'Partial upload',
        UPLOAD_ERR_NO_FILE=>'No file',
        UPLOAD_ERR_NO_TMP_DIR=>'No temporary directory',
        UPLOAD_ERR_CANT_WRITE=>'Can\'t write to disk',
        UPLOAD_ERR_EXTENSION=>'File upload stopped by extension'
    );


    $error = $_FILES['mail_list_file']['error'];
    $message = $upload_errors[$error];


    $list_file_name=$_FILES['mail_list_file']['name'];              //File Name
    $list_file_temp_name=$_FILES['mail_list_file']['tmp_name'];     // Temp Name


    if(move_uploaded_file($list_file_temp_name, 'uploades/'.$list_file_name)){ 

      //Uploaded File Moved Successfully
        $email_list = csv_to_array('uploades/'.$list_file_name);  // CSV data into array
        $total_list = sizeof($email_list);                        // Size of data

        for ($i=0; $i <$total_list ; $i++) {

            $mail_address = $email_list[$i]['Email Address'];
            
            $insertion_message[$i] = check_and_add_email_db('email',$mail_address,$_POST['client_group']);
         } 

    }else{
        echo "File upload failed!";
    }
}


 ?>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <!-- Add your site or application content here -->
        <header>
            <div class="container">
                <div class="logo">
                    <img src="img/logo.jpg" alt="Eagle Empire Pacific Ltd.">
                </div>
                <nav class="navbar navbar-default" role="navigation">
                    <ul class="nav navbar-nav">
                      
                      <li>
                        <a href="http://eagle-empire.com">
                         EEPL Home
                        </a>
                      </li>
                      <li>
                        <a href="http://support.eagle-empire.com">
                         Support Center
                        </a>
                      </li>
                    </ul>
                </nav>
            </div><!-- .container -->

        </header>

        <div class="container">

            <div class="row">

                <div class="col-md-4">

                    <?php get_sidebar(); ?>

                </div><!-- col-md-4 -->

                <div class="col-md-8">
                    <div class="main-content">
                      <?php 

                         if(!empty($insertion_message)){ ?>


                            <div class="panel panel-primary">
                              <div class="panel-heading">
                                <h3 class="panel-title">Attention!</h3>
                              </div>
                              <div class="panel-body">
                                <?php 

                                  echo "<ol>";
                                  
                                  for ($i=0; $i <sizeof($insertion_message); $i++) { 
                                    
                                    echo "<li>".$insertion_message[$i]."</li>";
                                  
                                  }
                                  
                                  echo "</ol>";
                                ?>

                              </div>
                            </div>


                         <?php 
                         
                         }
                        
                       ?>
                        <form class="form-horizontal" action="import_mail.php" enctype="multipart/form-data" role="form" method="post" id="mail-form">
                            

                            <div class="form-group">
                              <label for="client_group" class="col-sm-3 control-label">Newsletter List</label>
                              <div class="col-sm-9">
                                <select name="client_group" id="client_group" class="form-control" required>


                                  <?php 

                                      $client_group_query = mysql_query("SELECT * FROM client_group");
                                        if (!$client_group_query) {
                                          die("Database query failed");
                                        }

                                        while ($client_group_row=mysql_fetch_array($client_group_query )) {
                                          echo '<option value="';
                                          echo $client_group_row['id'];
                                          echo '">';
                                          echo ucfirst($client_group_row['group_name']);
                                          echo'</option>';
                                      }

                                   ?>
                                  
                                </select>
                              </div>
                          </div><!-- form-group -->

                            <div class="form-group">
                                <label for="mail_list_file" class="col-sm-3 control-label">Upload Contact List</label>
                                <div class="col-sm-9">
                                  <p><input type="file" name="mail_list_file" id="mail_list_file"></p>
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                  <input type="submit" name="mail_list_file_upload" class="btn btn-primary" value="Import">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div><!-- row -->
            
        </div><!-- container -->
		

<?php require_once("footer.php"); ?>