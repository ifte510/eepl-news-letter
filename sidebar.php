<div class="sidebar">
    <nav>
    	<ul>
            <li>
                <a href="index.php">Send Mail</a>
            </li>
            <li>
                <a href="add_department.php">Add Department</a>
            </li>
    		<li>
    			<a href="import_mail.php">Import Mail Addresses From CSV</a>
    		</li>
    		<li>
                <a href="new_single_mail.php">Add New Mail Address</a>
            </li>
            <li>
                <a href="create_mail_list.php">Create Newsletter List</a>
            </li>
            <li>
                
    			<a href="mail_list.php">Email Address List</a>
    		</li>
    	</ul>
    </nav>
</div><!-- sidebar -->