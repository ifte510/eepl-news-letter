<?php require_once("functions.php"); ?>
<?php require_once("header.php"); ?>
<?php require_once("csv_to_array.php"); ?>


<?php 


if(isset($_POST['add_mail_submit'])){

  $mail_address = $_POST['new_mail_add'];
  $mail_address_group = $_POST['client_group'];

  $insertion_message[0] = check_and_add_email_db('email',$mail_address,$_POST['client_group']);

}


 ?>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <!-- Add your site or application content here -->
        <header>
            <div class="container">
                <div class="logo">
                    <img src="img/logo.jpg" alt="Eagle Empire Pacific Ltd.">
                </div>
                <nav class="navbar navbar-default" role="navigation">
                    <ul class="nav navbar-nav">
                      
                      <li>
                        <a href="http://eagle-empire.com">
                         EEPL Home
                        </a>
                      </li>
                      <li>
                        <a href="http://support.eagle-empire.com">
                         Support Center
                        </a>
                      </li>
                    </ul>
                </nav>
            </div><!-- .container -->

        </header>

        <div class="container">

            <div class="row">

                <div class="col-md-4">

                    <?php get_sidebar(); ?>

                </div><!-- col-md-4 -->

                <div class="col-md-8">
                    <div class="main-content">

                      <?php 

                         if(!empty($insertion_message)){ ?>


                            <div class="panel panel-primary">
                              <div class="panel-heading">
                                <h3 class="panel-title">Attention!</h3>
                              </div>
                              <div class="panel-body">
                                <?php 

                                  echo "<ol>";
                                  
                                  for ($i=0; $i <sizeof($insertion_message); $i++) { 
                                    
                                    echo "<li>".$insertion_message[$i]."</li>";
                                  
                                  }
                                  
                                  echo "</ol>";
                                ?>

                              </div>
                            </div>


                         <?php 
                         
                         }
                        
                       ?>
                        <form class="form-horizontal" action="new_single_mail.php" role="form" method="post" id="add_mail_form">
                            

                            <div class="form-group">
                              <label for="client_group" class="col-sm-3 control-label">Newsletter List</label>
                              <div class="col-sm-9">
                                <select name="client_group" id="client_group" class="form-control" required>

                                   <?php 

                                      $client_group_query = mysql_query("SELECT * FROM client_group");
                                        if (!$client_group_query) {
                                          die("Database query failed");
                                        }

                                        while ($client_group_row=mysql_fetch_array($client_group_query )) {
                                          echo '<option value="';
                                          echo $client_group_row['id'];
                                          echo '">';
                                          echo ucfirst($client_group_row['group_name']);
                                          echo'</option>';
                                      }

                                   ?>
                                  
                                </select>
                              </div>
                          </div><!-- form-group -->

                            <div class="form-group">
                                <label for="new_mail_add" class="col-sm-3 control-label">Email Address</label>
                                <div class="col-sm-9">
                                  <p><input type="text" name="new_mail_add" id="new_mail_add" class="form-control" placeholder="Email Address"></p>
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                  <input type="submit" name="add_mail_submit" class="btn btn-primary" value="Include">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div><!-- row -->
            
        </div><!-- container -->
    

<?php require_once("footer.php"); ?>